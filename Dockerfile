FROM php:latest
WORKDIR /App
COPY . .
RUN apt-get update -yqq &&  \
    apt-get install -yqq git unzip &&  \
    curl -sS https://getcomposer.org/installer | php
RUN mv composer.phar /usr/local/bin/composer &&  \
    composer install --prefer-dist --no-interaction && \
    composer require friendsofphp/php-cs-fixer

EXPOSE 80
CMD [ "php", "-S", "0.0.0.0:80", "-t", "/App" ]
